import wget
import pathlib
import sys
import uuid
import json
import os


##mydir = os.path.dirname(__file__) or '.' 
##inpath = os.path.join(mydir, 'Input', 'F_ARMS_MST.json')
##filename = os.path.splitext(os.path.basename(__file__))[0]
##outpath = os.path.join(mydir, "Resources", filename+".json")

with open("FileList.json") as f:
    data = json.load(f)
    for item in data:
        print(item.split('/')[5]+"-"+item.split('/')[6])
        pathlib.Path(str(item.split('/')[5])).mkdir(parents=True,exist_ok=True)            
        wget.download(item,item.split('/')[5]+"/"+item.split('/')[6])

print("Done")
